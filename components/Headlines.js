import Link from 'next/link'
import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

const HEADLINES_PER_PAGE = 10;

function Headlines ({
  data: { loading, error, headline: { results } },
  loadMoreHeadlines
}) {
  if (error) return <div>Error</div>
  return (
    <section>           
      {results.map(result => (
      <div className="o-grid-row" key={result.id}>
        <div data-o-grid-colspan="full-width">
        <blockquote className="o-quote o-quote--standard">
          <p><a href={result.url}>{result.title}</a></p>
          <cite className="o-quote__cite"><span className="o-quote__author">{result.editorial}</span><span className="o-quote__source">{result.date}</span></cite>
        </blockquote><br />
        </div>
      </div>))}
      <div className="o-grid-row">      
        <button data-o-grid-colspan="one-third center" className="o-buttons o-buttons--secondary" onClick={() => loadMoreHeadlines()}>
          {' '}
          {loading ? 'Loading...' : 'Show More'}{' '}
        </button>
      </div>
    </section>
  )
}

export const allHeadlines = gql`
query($queryString: String, $maxResults: Int, $offset: Int) {
  headline(queryString: $queryString, maxResults: $maxResults, offset: $offset) {
    headlineQuery {
      offset,
      queryString
    },
    results {
      id,
      aspectSet,
      title,
      summary,
      date,
      url,
      editorial
    }
  }
}
`;

export const allHeadlinesQueryVars = {
  queryString: "",
  offset: 0,
  maxResults: HEADLINES_PER_PAGE
}

export default graphql(allHeadlines, {
  options: {
    variables: allHeadlinesQueryVars
  },
  props: ({ data }) => ({
    data,
    loadMoreHeadlines: () => {
      return data.fetchMore({
        variables: {
          queryString: data.headline.headlineQuery.queryString,
          offset: data.headline.results.length
        },
        updateQuery: (previousResult, { fetchMoreResult }) => {
          if (!fetchMoreResult) {
            return previousResult;
          }
          return Object.assign({}, previousResult, {
            headline: {
              results: [...previousResult.headline.results, ...fetchMoreResult.headline.results]
            }
          });
        }
      })
    }
  })
})(Headlines);
