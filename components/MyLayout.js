import Header from './Header';
import Head from 'next/head';

const Layout = (props) => (
  <div>
    <Head>
      <title>FT Headlines</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link rel="stylesheet" href="https://build.origami.ft.com/v2/bundles/css?modules=o-header-services@^2.0.5,o-forms@^5.2.3,o-buttons@^5.9.0,o-grid@^4.3.8,o-quote@^2.1.3" />
    </Head>
    <div>
      <header className="o-header-services" data-o-component="o-header">
        <div className="o-header-services__top o-header-services__container">
          <div className="o-header-services__ftlogo"></div>
          <div className="o-header-services__title">
            <h1 className="o-header-services__product-name"><a href="/">FT Headlines</a></h1
            ><span className="o-header-subrand__product-tagline ">browse and search</span>
          </div>
        </div>
        <Header />      
      </header>
      <div className="o-grid-container o-grid-container--bleed">
        {props.children}
      </div>
    </div>
  </div>
)

export default Layout