import { graphql } from 'react-apollo';
import gql from 'graphql-tag';
import { allHeadlines, allHeadlinesQueryVars } from './Headlines';

function Search({
  data: { refetch, loading, error, headline: { results } },
  searchHeadlines
}) {
  function handleSearch(event) {
    event.preventDefault();
    const form = event.target;
    const formData = new window.FormData(form);

    searchHeadlines(formData.get('queryString'));
  }

  return (
    <div className="o-grid-row">
    <div data-o-grid-colspan="one-third center">
        <form className="o-forms" onSubmit={handleSearch}>
          <div className="o-forms__affix-wrapper">
            <input className="o-forms__text o-forms__text--small" placeholder='search' name='queryString' type='text'/>
            <div className="o-forms__suffix">
              <button className="o-buttons o-buttons--secondary" type='submit'>Search</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  )
}

export default graphql(allHeadlines, {
  options: {
    variables: allHeadlinesQueryVars
  },
  props: ({ data }) => ({
    data,
    searchHeadlines: (queryString) => {
      return data.fetchMore({
        variables: {
          queryString: queryString,
          offset: 0
        },
        updateQuery: (previousResult, { fetchMoreResult }) => {
          if (!fetchMoreResult) {
            return previousResult;
          }
          return fetchMoreResult;
        }
      });
    }
  })
})(Search);