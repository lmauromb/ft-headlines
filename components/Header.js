import Link from 'next/link'
import Search from './Search';

const Header = () => (
    <div className="o-header-services__primary-nav">
		<div className="o-header-services__container">
			<ul className="o-header-services__nav-list">
				<li className="o-header-services__nav-item">
          <Link href="/">
            <a className="o-header-services__nav-link o-header-services__nav-link--selected">
              Headlines
            </a>
          </Link>
				</li>
				<li className="o-header-services__nav-item">
          <Link href="/about">
            <a className="o-header-services__nav-link">
              About
            </a>
          </Link>
				</li>
			</ul>
		</div>
	</div>
)

export default Header