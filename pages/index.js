import Layout from '../components/MyLayout';
import Headlines from '../components/Headlines';
import Search from '../components/Search';
import withData from '../lib/withData';

export default withData(() => (
  <Layout>
    <Search />
    <Headlines />
  </Layout>
));