import Layout from '../components/MyLayout';

export default () => (
  <Layout>
    <p> FT Headlines is a project developed for the technical frontend test.</p>
    <p> Author: Luis Mauro Martinez Baez (mauromb3@gmail.com)</p>
    <p> GitHub/GitLab/Twitter: @lmauromb </p>
    <h3> Check the Repos: </h3>
    <ul>
      <li><a href="https://gitlab.com/lmauromb/ft-headlines">This site.</a></li>
      <li><a href="https://gitlab.com/lmauromb/ft-graphql">GraphQL server.</a></li>
    </ul>      
  </Layout>
)