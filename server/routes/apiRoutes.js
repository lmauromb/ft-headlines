const express = require('express');
const router = express.Router();

const request = require('superagent');
const async = require('async');

router.get('/headlines', (req, res) => {
  request
  .post('https://api.ft.com/content/search/v1')
  .send({
    "queryString": ""   ,
    "resultContext" : {
      "aspects" :[  "title","lifecycle","location","summary","editorial" ],
      "sortOrder" : "DESC",
      "sortField" : "lastPublishDateTime",
      "maxResults" : 50
    }
  }) // sends a JSON post body
  .set('X-Api-Key', '59cbaf20e3e06d3565778e7bf73b92c3e7e941c8b41a27ab5f55113c')
  .set('Content-Type', 'application/json')
  .end((err, res) => {
    if (err) {
      return res.status(500).json(err);
    }
    res.status(200).json(response.body);
  });
});